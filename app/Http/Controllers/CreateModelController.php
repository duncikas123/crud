<?php

namespace App\Http\Controllers;

use App\Models\Fields;
use App\Models\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;


class CreateModelController extends Controller
{
    public function index()
    {
        return view('CreateModel');

    }

    public function insert(Request $request)
    {

        $rules = [
            'Model_Name' => 'required|unique:App\Models\Models|max:8'

        ];
        $attributes = [
            'Model_Name' => 'Formos Pavadinimas'

        ];
        $this->validate($request, $rules,[], $attributes);



        if ($request->Migration == 'false') {
            $migrs = 0;
        } else {
            $migrs = 1;
        }

        $model = new Models();
        $model->User_Id = Auth::user()->id;
        $model->Model_Name = $request['Model_Name'];
        $model->Migration = $migrs;
        $model->save();


        foreach ($request['Field_Name'] as $key => $row) {
            if ($request->Fill[$key] == 'false') {
                $fills = 0;
            } else {
                $fills = 1;
            }

            $field = new Fields();
            $field->Model_Id = $model->id;
            $field->Field_Name = $request->Field_Name[$key];
            $field->Simple_Name = $request->Simple_Name[$key];
            $field->DB_Type = $request->DB_Type[$key];
            $field->Validations = json_encode($request->Validations[$key]);
            $field->Fill = $fills;
            $field->save();


        }

        $this->controller($request->Model_Name, $request->Validations, $request->Simple_Name, $request->Field_Name);
        $this->view($request->Model_Name);
        $this->route($request->Model_Name);
        $this->model($request->Model_Name, $request->Field_Name, $request->Fill);
       $migration_file = $this->migration(
            $request->Model_Name,
            $request->Migration,
            $request->Field_Name,
            $request->DB_Type
            );
       $model->Migration_Name = $migration_file;
       $model->save();





        return redirect('Models')->with('success', 'Forma išsaugota');


    }

    protected function getStub($type)
    {

        return file_get_contents(resource_path("stubs/$type.sub"));

    }

    protected function model($Model_Name, $Field_Name, $Fill)
    {

        $fillable = [];
        foreach ($Fill as $key => $item) {
            if ($item == 'true') {
                $fillable[] = "'" . $Field_Name[$key] . "'";
            }
        }
        $string_for_change = implode(", ", $fillable);
        $Fields_Model = str_replace(
            [
                '$MODEL_CONTENT$',
                '{{modelName}}'
            ],
            [
                $string_for_change,
                $Model_Name
            ],
            $this->getStub('Model')
        );
        file_put_contents(app_path("Models/$Model_Name.php"), $Fields_Model);
    }


            protected function controller($Model_Name, $Validations, $Simple_Name, $Field_Name)
            {

                $vali = '';
                foreach ($Validations as $key => $item) {

                    $vali .= "'" .$Field_Name[$key]. "'". ' => ' . json_encode($Validations[$key]) . "," . "\n";

                }


                $simp = '';
                foreach ($Validations as $key => $item) {

                    $simp .= "'" .$Field_Name[$key]. "'". ' => ' ."'" . $Simple_Name[$key] . "'" . "," . "\n";

                }
                $Template = str_replace(
                    [
                        '{{modelName}}',
                        '{{modelNamePluralLowerCase}}',
                        '{{modelNameSingularLowerCase}}',
                        '$validations$',
                        '$simple$'
                    ],
                    [
                        $Model_Name,
                        strtolower(str::plural($Model_Name)),
                        strtolower($Model_Name),
                        $vali,
                        $simp
                    ],
                    $this->getStub('Controller')
                );

                file_put_contents(app_path("/Http/Controllers/{$Model_Name}Controller.php"), $Template);
            }



       protected function migration($Model_Name,$Migration,$Field_Name,$DB_Type)
        {
            $classname = 'Create'. ucfirst($Model_Name) . 'Table';

            $fields = '';
            foreach ($Field_Name as $key => $item) {

                $fields .= '$table' . '->' . $DB_Type[$key] . '(' ."'". $Field_Name[$key] ."'".')' . ";"."\n";

            }

            $Fields_Model = str_replace(
                [
                    '$MIGRATION_CONTENT$',
                    '{{modelName}}',
                    '{{classname}}'
                ],
                [
                    $fields,
                    $Model_Name,
                    $classname
                ],
                $this->getStub('Migration')
        );

       $file = "/migrations/" . date('Y_m_d_His') . "_create_{$Model_Name}_table.php";

                file_put_contents(database_path($file), $Fields_Model);



                if ($Migration=='true')
                {
                    Artisan::call('migrate ');

                }

            return $file;

        }




    protected function view($Model_Name)
     {
         $Template = str_replace(
             [
                 '{{modelName}}',

             ],
             [
                 $Model_Name,

             ],
             $this->getStub('View')
         );

         file_put_contents(resource_path("/views/{$Model_Name}.blade.php"), $Template);
     }

    protected function route($Model_Name)
 {



     File::append(base_path('routes/web.php'), 'Route::resource(\'' . str::plural(strtolower($Model_Name)) . "', '{$Model_Name}Controller');");


 }



}
