<?php

namespace App\Http\Controllers;

use App\Models\Models;
use App\Models\Fields;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class ModelsController extends Controller
{
    public function index()
    {

        $models = Models::where('User_Id', Auth::user()->id)->get();


        return view('Models', ['Models' => $models]);

    }

    public function search()
    {

        $search_text = $_GET['query'];
        $models = Models::where('Model_Name', 'LIKE', '%' . $search_text . '%')->where('User_Id', Auth::user()->id)->get();


        return view('models', ['Models' => $models]);

    }

    public function edit($id)
    {
        $model = Models::find($id);

        if ($model->User_Id == Auth::user()->id) {

            $fields = Fields::where('Model_Id', $model->id)->get();
            $db_valid = [];
            foreach ($fields as $field) {

                $db_valid[$field->Id] = json_decode($field->Validations);

            }
            return view('edit', ['model' => $model, 'fields' => $fields, 'db_valid' => $db_valid]);
        } else {

            echo 'Klaida';
            die();

        }
    }


    public function update(Request $request, $id)
    {
        Fields::where('Model_Id', $id)->delete();



        if ($request->Migration == 'false') {
            $migrs = 0;
        } else {
            $migrs = 1;
        }

        $modelis = Models::find($id);
        $modelis->Model_Name = $request['Model_Name'];
        $modelis->Migration = $migrs;
        $modelis->save();

        foreach ($request['Field_Name'] as $key => $row) {

            if ($request->Fill[$key] == 'false') {
                $fills = 0;
            } else {
                $fills = 1;
            }

            $fieldas = new Fields();
            $fieldas->Model_Id = $id;
            $fieldas->Field_Name = $request->Field_Name[$key];
            $fieldas->Simple_Name = $request->Simple_Name[$key];
            $fieldas->DB_Type = $request->DB_Type[$key];
            $fieldas->Validations = json_encode($request->Validations[$key]);
            $fieldas->Fill = $fills;
            $fieldas->save();
        }
        $migration_file = $this->fileupdate(

            $request->id,
            $request->Model_Name,
            $request->Validations,
            $request->Simple_Name,
            $request->Field_Name,
            $request->Fill,
            $request->Migration,
            $request->DB_Type
        );
        $modelis->Migration_Name = $migration_file;
        $modelis->save();



        return redirect(route('Models'))->with('success', 'Įrašas Redaguotas');

    }

    protected function getStub($type)
    {

        return file_get_contents(resource_path("stubs/$type.sub"));

    }
    public function fileupdate( $id, $Model_Name, $Validations, $Simple_Name, $Field_Name, $Fill, $Migration, $DB_Type)

    {

        var_dump($model->Migration_Name);
        die();
        if ($id != null)

        {
            $model = Models::find($id);

            unlink(app_path("/Http/Controllers/{$model->Model_Name}Controller.php"));
            unlink(app_path("Models/{$model->Model_Name}.php"));
            unlink(database_path($model->Migration_Name));
            unlink(resource_path("/views/{$model->Model_Name}.blade.php"));

        }

        $fillable = [];
        foreach ($Fill as $key => $item) {
            if ($item == 'true') {
                $fillable[] = "'" . $Field_Name[$key] . "'";
            }
        }
        $string_for_change = implode(", ", $fillable);
        $Fields_Model = str_replace(
            [
                '$MODEL_CONTENT$',
                '{{modelName}}'
            ],
            [
                $string_for_change,
                $Model_Name
            ],
            $this->getStub('Model')
        );
        file_put_contents(app_path("Models/$Model_Name.php"), $Fields_Model);

        $vali = '';
        foreach ($Validations as $key => $item) {

            $vali .= "'" .$Field_Name[$key]. "'". ' => ' . json_encode($Validations[$key]) . "," . "\n";

        }


        $simp = '';
        foreach ($Validations as $key => $item) {

            $simp .= "'" .$Field_Name[$key]. "'". ' => ' ."'" . $Simple_Name[$key] . "'" . "," . "\n";

        }
        $Template = str_replace(
            [
                '{{modelName}}',
                '{{modelNamePluralLowerCase}}',
                '{{modelNameSingularLowerCase}}',
                '$validations$',
                '$simple$'
            ],
            [
                $Model_Name,
                strtolower(str::plural($Model_Name)),
                strtolower($Model_Name),
                $vali,
                $simp
            ],
            $this->getStub('Controller')
        );

        file_put_contents(app_path("/Http/Controllers/{$Model_Name}Controller.php"), $Template);


        $classname = 'Create'. ucfirst($Model_Name) . 'Table';

        $fields = '';
        foreach ($Field_Name as $key => $item) {

            $fields .= '$table' . '->' . $DB_Type[$key] . '(' ."'". $Field_Name[$key] ."'".')' . ";"."\n";

        }

        $Fields_Model = str_replace(
            [
                '$MIGRATION_CONTENT$',
                '{{modelName}}',
                '{{classname}}'
            ],
            [
                $fields,
                $Model_Name,
                $classname
            ],
            $this->getStub('Migration')
        );

        $file = "/migrations/" . date('Y_m_d_His') . "_create_{$Model_Name}_table.php";

        file_put_contents(database_path($file), $Fields_Model);



        if ($Migration=='true')
        {
            Artisan::call('migrate ');

        }



        $Template = str_replace(
            [
                '{{modelName}}',

            ],
            [
                $Model_Name,

            ],
            $this->getStub('View')
        );

        file_put_contents(resource_path("/views/{$Model_Name}.blade.php"), $Template);

        return $file;

    }

    public function destroy($id)
    {
        if ($id != null)

        {
            $model = Models::find($id);

            unlink(app_path("/Http/Controllers/{$model->Model_Name}Controller.php"));
            unlink(app_path("Models/{$model->Model_Name}.php"));
            unlink(database_path($model->Migration_Name));
            unlink(resource_path("/views/{$model->Model_Name}.blade.php"));

            Models::where('id', $id)->delete();
        }


        return redirect('Models');

    }



}
