<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Jūsų slaptažodis buvo iš naujo nustatytas!',
    'sent' => 'El. Paštu atsiųsime slaptažodžio nustatymo nuorodą!',
    'throttled' => 'Palaukite prieš bandydami dar kartą.',
    'token' => 'Šis slaptažodžio nustatymo prieigos raktas netinkamas.',
    'user' => "Neįmanoma rasti vartotojo, turinčio šį el. Pašto adresą.",

];
