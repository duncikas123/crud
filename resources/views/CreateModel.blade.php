@extends('layouts.app')

@section('additional_header_content')

@endsection

@section('content')
    @include('users.partials.header', ['title' => __('Sukurti Formą'),
             'class' => 'col-lg-12'])

    <html>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>


    <div class="card card-stats mt-0 xl-200 bg-gradient-cyan">
        <form id="form-group col-md-6" name="ConTable" action="{{url('CreateModel')}}" method="post"
              class="basic_steps">
            @csrf
            <div class="container">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row align-items-start">
                    <div class="col">
                        <div class="form-group col-md-4 text-darker" style="padding-top: 20px;">
                            <label for="txtModelName">Formos pavadinimas<span class="required">*</span></label>
                            <input type="text" class="form-control text-capitalize" name="Model_Name" id="txtModelName"
                                   placeholder="Pvz.: Modelis" required="required">
                        </div>
                    </div>

                    <div class="col-md-auto">
                        <div class="custom-control custom-checkbox text-darker" style="padding-top: 60px">
                            <input type="hidden" name="Migration" value="true">
                            <input type="checkbox" onclick="$(this).prev('input').val($(this).is(':checked'))" class=""
                                   checked="checked">
                            <label for="customCheck1">Migracija</label>
                        </div>
                    </div>
                    <div class="col col-lg-2">
                        <div class="form-inline col-md-12">
                            <div class="form-group chk-align" style="border-color: transparent;padding-top: 50px">
                                <button type="button" class="btn btn-darker" id="btnAdd"> Pridėti lauką
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-2 mb-3">
                <div class="table-responsive col-md-12">
                    <table class="table table-striped table-bordered bg-info shadow " id="table">
                        <thead class="no-border">
                        <tr>
                            <th class="text-darker">Programinis lauko pavadinimas*</th>
                            <th class="text-darker">Lauko pavadinimas*</th>
                            <th class="text-darker">DB tipas*</th>
                            <th class="text-darker">Validacijos*</th>
                            <th class="text-darker" style="width: 63px">Užpildomas(ang. Fillable)*</th>
                            <th class="text-darker">Ištrinti</th>
                        </tr>
                        </thead>

                        <tbody id="container" class="no-border-x no-border-y ui-sortable">
                        <div class="myClass">
                            <th style="vertical-align: middle">
                                <input type="text" name="Field_Name[1]" placeholder="Įvesti programinį vardą"
                                       style="width: 100%" class="form-control txtFieldName error" required="required">

                            </th>
                            <th style="vertical-align: middle">
                                <input type="text" name="Simple_Name[1]" placeholder="Vardas" style="width: 100%"
                                       class="form-control txtFieldName error" required="required" >
                            </th>
                            <th style="vertical-align: middle">
                                <select class="form-control txthbType select2-hidden-accessible" name="DB_Type[1]"
                                        data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option value="increments" data-select2-id="57">INCREMENTS</option>
                                    <option value="bigincrements" data-select2-id="58">BIG INCREMENTS</option>
                                    <option value="timestamps" data-select2-id="59">TIME STAMPS</option>
                                    <option value="softdeletes" data-select2-id="60">SOFT DELETES</option>
                                    <option value="remembertoken" data-select2-id="61">REMEMBER TOKEN</option>
                                    <option disabled="disabled" value="disabled" data-select2-id="62">-</option>
                                    <option value="string"  data-select2-id="3">STRING</option>
                                    <option value="text" class="text" data-select2-id="63">TEXT</option>
                                    <option disabled="disabled" value="disabled" data-select2-id="64">-</option>
                                    <option value="tinyinteger" data-select2-id="65">TINY INTEGER</option>
                                    <option value="smallinteger" data-select2-id="66">SMALL INTEGER</option>
                                    <option value="mediuminteger" data-select2-id="67">MEDIUM INTEGER</option>
                                    <option value="integer" data-select2-id="68">INTEGER</option>
                                    <option value="biginteger" data-select2-id="69">BIG INTEGER</option>
                                    <option disabled="disabled" value="disabled" data-select2-id="70">-</option>
                                    <option value="float" data-select2-id="71">FLOAT</option>
                                    <option value="decimal" data-select2-id="72">DECIMAL</option>
                                    <option value="boolean" data-select2-id="73">BOOLEAN</option>
                                    <option disabled="disabled" value="disabled" data-select2-id="74">-</option>
                                    <option value="enum" data-select2-id="75">ENUM</option>
                                    <option disabled="disabled" value="disabled" data-select2-id="76">-</option>
                                    <option value="date" data-select2-id="77">DATE</option>

                                </select>

                            </th>

                            <th style="vertical-align: middle">
                                <select class="form-control validation select2-hidden-accessible"
                                        name="Validations[1][]" multiple="multiple" required="required">
                                    <option value="Required" class="Required">Required</option>
                                    <option value="Email" class="Email">Email</option>
                                    <option value="Array" class="Array">Array</option>
                                    <option value="Unique" class="Unique">Unique</option>
                                    <option value="String" class="String">String</option>
                                    <option value="Image" class="Image">Image</option>
                                    <option value="Integer" class="Integer">Integer</option>
                                    <option value="Boolean" class="Boolean">Boolean</option>

                                </select>
                            </th>


                            <th style="text-align: center;vertical-align: middle; padding-bottom: 15px;">
                                <div class="custom-control custom-checkbox checkbox-xl" style="text-align: center">
                                    <input
                                        style="width: 30px;height: 30px;padding: 6px; border-radius: 15px;text-align: center; font-size: 12px; line-height: 1.42857;"
                                        type="hidden" name="Fill[1]" value="true">
                                    <input type="checkbox" onclick="$(this).prev('input').val($(this).is(':checked'))"
                                           class="" checked="checked">
                                </div>
                            </th>


                            <th style="text-align: center;vertical-align: middle; padding-bottom: 15px;">
                                <div>
                                    <button type="button"
                                            style="width: 30px;height: 30px;padding: 6px; border-radius: 15px;text-align: center; font-size: 12px; line-height: 1.42857;"
                                            id="'+i+'" class="btn btn-warning btn-circle"><i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </th>

                        </div>
                        </tbody>
                    </table>
                    <div class="container">
                        <div class="row">
                            <div class="col-400">
                                <div class="form-inline col-md-12 div_gnr_rst " style="padding-top: 10px">
                                    <div class="form-group btn_generate">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block btn-darker"
                                                id="btnGenerate">Išsaugoti
                                        </button>
                                        <h6>* privaloma užpildyti</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(document).ready(function () {
                    let i = 1;
                    $('#btnAdd').click(function () {
                        i++;
                        $('#table').append('<tr id="row' + i + '"><th style="vertical-align: middle"> <input type="text" name="Field_Name[' + i + ']" placeholder="Įvesti programinį vardą" style="width: 100%" required="required" class="form-control txtFieldName error" aria-required="true" aria-invalid="true"></th><th style="vertical-align: middle"> <input type="text" name="Simple_Name[' + i + ']" placeholder="Vardas" style="width: 100%" required="required" class="form-control txtFieldName error" aria-required="true" aria-invalid="true"/> </th><th style="vertical-align: middle"><select class="form-control txthbType select2-hidden-accessible" name="DB_Type[' + i + ']" data-select2-id="1" tabindex="-1" aria-hidden="true">  <option value="increments" data-select2-id="57">INCREMENTS</option> <option value="bigincrements" data-select2-id="58">BIG INCREMENTS</option> <option value="timestamps" data-select2-id="59">TIME STAMPS</option> <option value="softdeletes" data-select2-id="60">SOFT DELETES</option> <option value="remembertoken" data-select2-id="61">REMEMBER TOKEN</option> <option disabled="disabled" value="disabled" data-select2-id="62">-</option> <option value="string" selected="selected" data-select2-id="3">STRING</option> <option value="text" class="text" data-select2-id="63">TEXT</option> <option disabled="disabled" value="disabled" data-select2-id="64">-</option> <option value="tinyinteger" data-select2-id="65">TINY INTEGER</option> <option value="smallinteger" data-select2-id="66">SMALL INTEGER</option> <option value="mediuminteger" data-select2-id="67">MEDIUM INTEGER</option> <option value="integer" data-select2-id="68">INTEGER</option> <option value="biginteger" data-select2-id="69">BIG INTEGER</option> <option disabled="disabled" value="disabled" data-select2-id="70">-</option> <option value="float" data-select2-id="71">FLOAT</option> <option value="decimal" data-select2-id="72">DECIMAL</option> <option value="boolean" data-select2-id="73">BOOLEAN</option> <option disabled="disabled" value="disabled" data-select2-id="74">-</option> <option value="enum" data-select2-id="75">ENUM</option> <option disabled="disabled" value="disabled" data-select2-id="76">-</option> <option value="date" data-select2-id="77">DATE</option></select><span class="select2 select2-container select2-container--default select2-container--above" dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-txthbType-w4-container"><span class="select2-selection__rendered" id="select2-txthbType-w4-container" role="textbox" aria-readonly="true" title="STRING"></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th><th style="vertical-align: middle"> <select class="form-control validation select2-hidden-accessible" name="Validations[' + i + '][]" multiple=""   required="required"  > <option value="Required" class="Required">Required</option><option value="Email" class="Email">Email</option><option value="Array" class="Array">Array</option><option value="Unique" class="Unique">Unique</option><option value="String" class="String">String</option><option value="Image" class="Image">Image</option><option value="Integer" class="Integer">Integer</option><option value="Boolean" class="Boolean">Boolean</option></select></th> <th style="text-align: center;vertical-align: middle; padding-bottom: 15px;"> <div class="custom-control custom-checkbox checkbox-xl" style="text-align: center" > <input type="hidden" name="Fill[' + i + ']" value="true"> <input type="checkbox" onclick ="' + "$(this).prev('input').val($(this).is(':checked'))" + '" class="" checked="checked"> </div></th><th style="text-align: center;vertical-align: middle; padding-bottom: 10px;width: 30px;"> <div> <button type="button" style="width: 30px;height: 30px;padding: 6px; border-radius: 15px;text-align: center; font-size: 12px; line-height: 1.42857;"  id="' + i + '" class="btn btn-warning btn-circle"><i class="fa fa-times"></i> </button> </div> </th></tr>');
                    });

                    $(document).on('click', '.btn-circle', function () {
                        let button_id = $(this).attr("id");
                        $('#row' + button_id + '').remove();

                    });


                });


            </script>
            {{ csrf_field() }}
        </form>
    </div>
    </html>



@endsection
