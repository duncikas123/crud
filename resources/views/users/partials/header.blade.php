<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url(../argon/img/theme/2372842.jpg); background-size: cover; background-position: center top; height: 150px">
    <!-- Mask -->
    <span class=" bg-gradient-default opacity-8"></span>
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center">
        <div class="row">
            <div class="col-md-12 {{ $class }}">
                @if(isset($title))
                <h2 class="display-2 text-white font-weight-normal">{{ $title }}</h2>
                @endif

            </div>
        </div>
    </div>
</div>
