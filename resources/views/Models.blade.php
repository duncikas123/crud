@extends('layouts.app')

@section('additional_header_content')

@endsection

@section('content')
    @include('users.partials.header', ['title' => __('Formos'),
             'class' => 'col-lg-12'])

    <form class="input-group" action="{{url('search')}}" method="get" >
        <input  type="search" class="form-control rounded" placeholder="Įvesti formos pavadinimą" name="query">
        <button type="submit" class="btn btn-outline-primary">Ieškoti</button>
    </form>

    <table class="table table-hover table-dark table table-bordered  ">
        <thead class="bg-darker">
        <tr>
            <th style="text-align: center">Formos Id</th>
            <th style="text-align: center">Formos Pavadinimas</th>
            <th style="text-align: center">Redaguoti</th>
            <th style="text-align: center">Ištrinti</th>
        </tr>
        </thead>
        @foreach($Models as $model)
            <div class="row">
                <tbody id="container" >
                <div class="myClass">
                    <th style="vertical-align: middle; text-align:center" >
                        <a class=" text-bold text-white">{{ $model->id }}</a>
                    </th>
                    <th style="vertical-align: middle; text-align:center" >
                        <a class=" text-bold text-white">{{ $model->Model_Name }}</a>
                    </th>

                    <th style="text-align: center;vertical-align: middle; padding-bottom: 10px;width: 30px;">

                        <form action="{{ route('models.edit', $model->id) }}" id="edit_model" method="POST">
                            @csrf
                            @method('GET')
                            <button type="submit" class="btn btn-blue btn-sm">Redaguoti</button>

                        </form>
                    </th>

                    <th style="text-align: center;vertical-align: middle; padding-bottom: 10px;width: 30px;">

                        <form action="{{ route('models.destroy', $model->id) }}" id="delete_model" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">Ištrinti</button>

                        </form>
                    </th>
                </div>
                </tbody>

            </div>
        @endforeach
    </table>
    </div>


@endsection

