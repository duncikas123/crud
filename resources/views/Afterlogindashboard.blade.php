@extends('layouts.app', ['title' => __('Naujienos')])

@section('additional_header_content')

@endsection

@section('content')
    @include('users.partials.header', ['title' => __('Pagrindinis'),
             'class' => 'col-lg-12'])

    <div class="card card-stats mt-0 xl-500 bg-gradient-cyan">

        <div class="form-group  text-darker text-center" style="padding-top: 20px;">
            <h1><b> <label>Jūs Prisijungėte! Pažiūrėję į kairę matysite galimus funkcionalumus</label></b></h1>

        </div>

    </div>



    </div>
@endsection
