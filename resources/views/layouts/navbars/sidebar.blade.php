<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-gradient-info" id="sidenav-main">
    <span class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{ route('home') }}">
            <img src="{{ asset('argon') }}/img/brand/blue.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                    </a>
                </span>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('argon') }}/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Navigation -->
            <ul class="navbar-nav">
                    @if(Auth::guest())
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">
                            <i class="ni ni-key-25 text-darker"></i> <b class="text-darker">{{ __('Prisijungti') }}</b>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">
                            <i class="ni ni-circle-08 text-darker"></i> <b class="text-darker"> {{ __('Registruotis') }}</b>
                        </a>
                    </li>
                    @else


                @endif


                        @if (Illuminate\Support\Facades\Auth::check() && Auth::user()->isRole()=="admin"  )

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('CreateModel') }}">
                                    <i class="ni ni-tv-2 text-darker"></i> <b class="text-darker">{{ __('CRUD generatorius') }}</b>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('Models') }}">
                                    <i class="ni ni-books text-darker"></i> <b class="text-darker">{{ __('Formos') }}</b>
                                </a>
                            </li>
                        @endif
            </ul>

        </div>
    </div>
</nav>
