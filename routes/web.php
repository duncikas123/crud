<?php

use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainPageController@index'  )->name('guest-page');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');



Route::group(['middleware' => 'auth'], function () {
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});
Route::group(['middleware' => ['auth'=>'admin']], function () {
    Route::get('CreateModel', 'CreateModelController@index')->name('CreateModel');
    Route::post('CreateModel', 'CreateModelController@insert');
    Route::get('Models', 'ModelsController@index')->name('Models');
    Route::resource('models', 'ModelsController');

    Route::get('Models/{id}/edit', 'ModelsController@edit')->name('models.edit');
    Route::put('Models/{id}', 'ModelsController@update')->name('models.update');

    Route::get('search', 'ModelsController@search');

});


Auth::routes();


Route::group(['middleware' => 'auth'], function () {
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});



Route::resource('modelis', 'ModelisController');Route::resource('modelis', 'modelisController');Route::resource('naujs', 'naujController');Route::resource('karts', 'kartController');