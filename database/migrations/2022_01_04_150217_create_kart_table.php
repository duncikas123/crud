<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateKartTable extends Migration
{
    public function up()
    {

        Schema::create('kart', function (Blueprint $table) {

        $table->increments('laukas');

        $table->timestamps();

        });
    }


    public function down()
    {
     Schema::dropIfExists('kart');
    }
}
