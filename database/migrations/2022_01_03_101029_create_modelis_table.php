<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateModelisTable extends Migration
{
    public function up()
    {

        Schema::create('modelis', function (Blueprint $table) {

        $table->increments('as');

        $table->timestamps();

        });
    }


    public function down()
    {
     Schema::dropIfExists('modelis');
    }
}
